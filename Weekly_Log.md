<h5><div align="right">

_AEROSPACE DESIGN PROJECT, EAS4947_<br>
_SEMESTER 1 2021/22_<br>
</div></h5>

**Name**: KISSHORE VARMA A/L RAJASEGARAN<br>
**Matric No**: 193569

## Table Of Content


<div align="center">

- [**WEEK 3**](#log-book-03)
- [**WEEK 4**](#log-book-04)
- [**WEEK 5**](#log-book-05)
- [**WEEK 6**](#log-book-06)
- [**WEEK 7**](#log-book-07)
- [**WEEK 8**](#log-book-08)
- [**WEEK 9**](#log-book-09)
- [**WEEK 10**](#log-book-10)
- [**WEEK 11**](#log-book-11)
- [**WEEK 12**](#log-book-12)
- [**WEEK 13**](#log-book-13)
- [**WEEK 14**](#log-book-14)

</div>

----


[**Table of Content**](#table-of-content)

---
<div align="right"><b> 07<sup>th</sup> November 2021</b></div>

<h1><div align="center"> LOG BOOK 03 </div></h1>

## Agenda and Goals
They were given two task on this week
* Revised control engineering basics in order to focus on dynamic system modelling and the construction of controllers that would cause the system to behave as desired.
* Revised the primary types of flight controllerssuc as PID used in dyanamic control systems to assess steady state and increase aerospace vehicle stability.
* Engineering Logbook. (team part)

## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
| | |

<div align="justify">
The reason I revised through on Control Engineering basics as it is a fundamental part in understanding the control and stability of finless UAV airships
</div>

## Impact
increase my understanding on control systems and why control and stability is important and its significants on reducing the cost of the consumption of the sprayer fluid, battery consumption and flight time.

## Next step
- Familiarize yourself with ArduPilot Mission Planner and everything that it has to offer for control and stability of finless UAVs


[**Table of Content**](#table-of-content)

---
<div align="right"><b> 14<sup>th</sup> November 2021</b></div>
<h1><div align="center"> LOG BOOK 04 </div></h1>

## Agenda and Goals
*  Firmware installation and initial calibrations before flight of airship
*  Reading and familiarising ourselves with the PX4 documentation via the PX4 user handbook; this is the firmware that we will be utilising for the HAU's autonomous flight.


## Problem and Descision taken
| Problem | Solution |
| ------ | ------ |
| Installing PX4 into using mission planner and conduct inital calibrations | Following this [Documentation ](http://pix.rctoysky.com/how-to-upload-firmware.html) on the installation of PX4  |


<div align="justify">
The Px4 firmware is installed using a Ground Station (GSC) software. Some of the more popular GCS systems are Mission Planner,QGround control and APM Planner 2. For this project, we will be using the Mission planner software as our GSC system to install the PX4 firmware. This GCS is the most compatible and closely tracks new features and updates in ArduPilot. It is recommended for first-time users and power users alike. It functions best in the Windows environment.
</div>


##### Steps to loading the PX4 firmware
1. Once you have installed a ground station on your computer, connect the autopilot using the micro USB cable to your computer. Use a direct USB port on your computer 
2. If using Mission Planner as the GCS, select the COM port drop-down in the upper-right corner of the window near the Connect button. Select AUTO or the specific port for your board. Set the Baud rate to 115200 as shown. Do not hit Connect just yet.
3. In Mission Planner’s SETUP | Install Firmware screen select the appropriate icon that matches your frame (i.e. Quad, Hexa). Answer Yes when it asks you “Are you sure?”. Mission Planner will try to detect which board you are using. It may ask you to unplug the board, press OK, and plug it back in to detect the board type.

If all goes well, you will see a status appear on the bottom right including the words: “erase…”, “program…”, “verify..”, and “Upload Done”. The firmware has been successfully uploaded to the board. It usually takes a few seconds for the bootloader to exit and enter the main code after programming or a power-up. Wait to press CONNECT until this occurs.
 
##### Testing
You can test the firmware is working on a basic level by switching to the Mission Planner Flight Data screen and pressing the Connect button. The HUD should update as you tilt the board.

## Impact
Learned how to do Install the PX4 Firmware into Mission planner and conduct testing on the functionality of the firmware

## Next step
* Conducting Ground calibration and checking the CG before and after the first flight
* Calibrate and check the Accelerometer Calibration, Radio Control Calibration Compass Calibration, RC Transmitter Mode Setup , ESC Calibration and Frame Class and Type Configuration.

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 21<sup>th</sup> November 2021</b></div>
<h1><div align="center"> LOG BOOK 05 </div></h1>

## Agenda and Goals
* Conducting Ground calibration and checking the CG before and after the first flight
* Calibrate and check the Accelerometer Calibration, Radio Control Calibration Compass Calibration, RC Transmitter Mode Setup , ESC Calibration and Frame Class and Type Configuration

## Problem and Descision taken
| Problem | Solution |
| ------ | ------ |
| Ground calibration | Set your transmitter mode switch to Manual. This is a safe mode in which to start up the system.When you power on your board at the field, you should leave the plane motionless on the ground until the LEDs stop flashing blue and red (about 30 seconds). This means that the gyros have been calibrated. [Reference](https://ardupilot.org/plane/docs/starting-up-and-calibrating-arduplane.html) |
| Calibrate and check the Accelerometer Calibration, Radio Control Calibration Compass Calibration, RC Transmitter Mode Setup and ESC Calibration| Documentation on [Accelerometer Calibration](http://pix.rctoysky.com/accelerometer-calibration.html), [Radio Control Calibration](http://pix.rctoysky.com/radio-control-calibration.html) [Compass Calibration](http://pix.rctoysky.com/compass-calibration.html), [RC Transmitter Mode Setup](http://pix.rctoysky.com/how-to-connect-remote-control-receiver.html), [ESC Calibration](http://pix.rctoysky.com/how-to.html) and [Frame Class and Type Configuration](http://pix.rctoysky.com/frame-class-and-type-configuration.html)|

<div align="justify">
These calibrations and testing are important to know and conduct to ensure accuracy of the dat they collect. The goal of calibration is to minimise any measurement uncertainty by ensuring the accuracy of test equipment. Calibration quantifies and controls errors or uncertainties within measurement processes to an acceptable level.

</div>

## Impact
* Learned the importance of ground calibration
* Learned the how to check and calibrate the Accelerometer Calibration, Radio Control Calibration Compass Calibration, RC Transmitter Mode Setup , ESC Calibration and Frame Class and Type Configuration using the mission planner software.

## Next step
* Basic Assembly of flight controller . These cover wiring of the core sensors to specific flight controller hardware.
* Wiring  of Flight Controller.


[**Table of Content**](#table-of-content)

---
<div align="right"><b> 26<sup>th</sup> November 2021</b></div>
<h1><div align="center"> LOG BOOK 06 </div></h1>

## Agenda and Goals
* Basic Assembly of flight controller . These cover wiring of the core sensors to specific flight controller hardware.
* Wiring  of Flight Controller.


## Problem and Descision taken
| Problem | Solution |
| ------ | ------ |
| Basic assembly and wiring of sensors to specific flight controller hardware  |Documentation on [sensors](https://docs.px4.io/master/en/getting_started/sensor_selection.html) involved and [flight control](https://docs.px4.io/master/en/getting_started/flight_controller_selection.html) selection.|


<div align="justify">
Sensor wiring information and datasheet is usually provided in manufacturer documentation for flight controllers and the sensors themselves. PX4 is available on many popular commercial drone products, including some that ship with PX4 and others that can be updated with PX4 (allowing you to add mission planning and other PX4 Flight modes to your vehicle), which is the reason why PX4 is used in this project.
</div>

## Impact
* Learned sensor wiring and basic assembly of flight controller

## Next step
* Radio Systems setup
* Tuning of the PID for the pitch and yaw motion
* Looking through codes ArduHAU for the airship and understand the necssary codes that will be in use

[**Table of Content**](#table-of-content)

---
<div align="right"><b> 2<sup>nd</sup> December 2021</b></div>
<h1><div align="center"> LOG BOOK 07 </div></h1>


## Agenda and Goals
* Radio Systems setup
* Tuning of the PID for the pitch and yaw motion
* Looking through codes ArduHAU for the airship and understand the necssary codes that will be in use



## Problem and descision taken
| Problem | Solution |
| ------ | ------ |
|Configuring your radio with PX4  | Documentation on how to calibrate and configure the RC system. [Radio calibration](http://pix.rctoysky.com/radio-control-calibration.html) |
|Set Signal-Loss Behaviour of RC  | The way to do this is to set the RC controller trim and throttle stick as low as possible, and use the resulting output PWM value in both PX4 and the receiver (read your receiver manual to determine how to set the RC loss value). Then reset the throttle stick trim back to its normal position. This process ensures that the RC loss value is below the minimum value output by the receiver in normal operation. |
|Tuning the Airship using mission planner  | Documentation on Tuning process instruction, Autotuning, Manual tuning for roll and pitch control and etc. [Tuning](https://ardupilot.org/copter/docs/common-tuning.html) |

<div align="justify">

The radio control is very important because it is where we control the movement of the airship directly. A radio control (RC) system is required if you want to manually control your vehicle from a handheld transmitter. It is aspecially important to know how the RC systems works, how to choose the appropriate radio system and connect it to your flight controller. learned little about how RC works, how to choose an appropriate radio system for our airship, and how to connect it to your flight controller. Now, when using the RC system, we also need to know if there has been a signal lost from the airship. That is why we need to set up a signal loss behaviour of the Rc system, where the system will give uas an indication whther there has been a signal lostfrom the airship.

Tuning is importance to ensure that the airship flies well, with tight navigation and reliable performance with or without the influnce of wind. For our project we will be conducting inflight tuning or manual tuning.Although there are many gains that can be tuned for the airship to get optimal performance, the most critical is the Rate Roll and Pitch P values which convert the desired rotation rate into a motor output. This alone will likely get it flying reasonably well at least in Stabilize mode.

Some general advice on how to tune this parameter:

- Too high and the UAV will oscillate quickly in roll and/or pitch
- Too low and the UAV will become sluggish
- High powered UAV should use a lower gain, under powered UAVs are a higher gain

</div>

## Impact
* Learned little about how RC works, how to choose an appropriate radio system for our airship, and how to connect it to your flight controller
* Learned to setup signal lost behaviour of RC system

## Next step
* Apply all that our subsystem has learned and developed, once we have recived the hardware for the airship

[**Table of Content**](#table-of-content)

---

<h1><div align="center"> LOG BOOK 08 </div></h1>

[**Table of Content**](#table-of-content)

---

<h1><div align="center"> LOG BOOK 09 </div></h1>

[**Table of Content**](#table-of-content)

---

<h1><div align="center"> LOG BOOK 10 </div></h1>

[**Table of Content**](#table-of-content)

---

<h1><div align="center"> LOG BOOK 11 </div></h1>

[**Table of Content**](#table-of-content)

---

<h1><div align="center"> LOG BOOK 12 </div></h1>

[**Table of Content**](#table-of-content)

---

<h1><div align="center"> LOG BOOK 13 </div></h1>

[**Table of Content**](#table-of-content)

---

<h1><div align="center"> LOG BOOK 14 </div></h1>

[**Table of Content**](#table-of-content)

---
